<?php
declare(strict_types=1);

namespace Profile\Lib\Consts;

class MainGoals
{
    const GET_FIRST_JOB = 'get_first_job';
    const GROW_CURRENT_ROLE = 'grow_current_role';
    const SWITCH_ROLE = 'switch_role';
    const LEARN_OUTSIDE_WORK = 'learn_outside_work';
}
