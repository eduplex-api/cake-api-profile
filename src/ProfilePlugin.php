<?php
declare(strict_types=1);

namespace Profile;

use Cake\Routing\RouteBuilder;
use RestApi\Lib\RestPlugin;

class ProfilePlugin extends RestPlugin
{
    protected function routeConnectors(RouteBuilder $builder): void
    {
        $builder->connect('/profiles/*', \Profile\Controller\UserProfilesController::route());
        $builder->connect('/profile/openapi/*', \Profile\Controller\SwaggerJsonController::route());
    }
}
