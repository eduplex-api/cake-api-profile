<?php
declare(strict_types=1);

namespace Profile\Model\Entity;

use Cake\ORM\Entity;

/**
 * @property mixed $user_id
 * @property string $current_occupation
 * @property string $target_occupation
 * @property string $main_goal
 * @property string $goal_description
 * @property string $industry
 * @property string $profile
 * @property mixed $created
 * @property mixed $modified
 * @property mixed $level
 * @property mixed $profiles_skills
 */
class UserProfile extends Entity
{
    protected $_accessible = [
        '*' => false,
        'id' => false,
        'user_id' => false,
        'created' => false,
        'modified' => false,
        'profile' => false,

        'current_occupation' => true,
        'target_occupation' => true,
        'main_goal' => true,
        'goal_description' => true,
        'industry' => true,
        'profiles_skills' => true,
        'level' => true,
    ];

    protected $_hidden = [
        'deleted'
    ];

    protected $_virtual = [
    ];

    public function getProfilesSkills(): array
    {
        return $this->_fields['profiles_skills'] ?? [];
    }
}
