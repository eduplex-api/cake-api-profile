<?php
declare(strict_types=1);

namespace Profile\Model\Table;

use Cake\ORM\Behavior\TimestampBehavior;
use RestApi\Model\Table\RestApiTable;

class ProfilesSkillsTable extends RestApiTable
{
    public static function load(): self
    {
        /** @var self $table */
        $table = parent::load();
        return $table;
    }

    public function initialize(array $config): void
    {
        $this->addBehavior(TimestampBehavior::class);
        UserProfilesTable::addHasMany($this)->setForeignKey('user_profile_id');
    }
}
