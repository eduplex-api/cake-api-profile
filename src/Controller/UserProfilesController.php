<?php
declare(strict_types=1);

namespace Profile\Controller;

use App\Controller\ApiController;
use Cake\Http\Exception\ForbiddenException;
use Profile\Model\Entity\UserProfile;
use Profile\Model\Table\UserProfilesTable;

/**
 * @property UserProfilesTable $Profiles
 */
class UserProfilesController extends ApiController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->Profiles = UserProfilesTable::load();
    }

    protected function getData($id)
    {
        $userId = $this->_validateAndGetUserId($id);
        $this->return = $this->Profiles->findUserProfileByUserId($userId)->firstOrFail();
    }

    protected function addNew($data)
    {
        $userId = $this->_validateAndGetUserId($data['user_id']);

        $profile = $this->Profiles->newEmptyEntity();
        $profile = $this->Profiles->patchEntity($profile, $data, ['associated' => ['ProfilesSkills']]);

        /** @var UserProfile $profile */
        $profile->user_id = $userId;
        $this->Profiles->addDeletingOldOrFail($profile);

        $this->return = $this->Profiles->findUserProfileByUserId($userId)->firstOrFail();
    }

    protected function delete($id)
    {
        $userId = $this->_validateAndGetUserId($id);
        $this->Profiles->deleteAllByUser($userId);
        $this->return = false;
    }

    private function _validateAndGetUserId($userId)
    {
        $tokenUserId = $this->OAuthServer->getUserID();
        if ($tokenUserId != $userId) {
            throw new ForbiddenException('Resource not allowed with this token');
        }
        return $tokenUserId;
    }
}
