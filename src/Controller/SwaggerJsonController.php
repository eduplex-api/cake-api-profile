<?php

declare(strict_types = 1);

namespace Profile\Controller;

use Profile\ProfilePlugin;

class SwaggerJsonController extends \RestApi\Controller\SwaggerJsonController
{
    protected function getContent(\RestApi\Lib\Swagger\SwaggerReader $reader, array $paths): array
    {
        $host = $_SERVER['HTTP_HOST'] ?? 'example.com';
        $plugin = ProfilePlugin::getRoutePath();
        $url = 'https://' . $host . $plugin . '/';
        return [
            'openapi' => '3.0.0',
            'info' => [
                'version' => '0.2.0',
                'title' => 'Profile plugin',
                'description' => 'Profile connector plugin',
                'termsOfService' => 'https://www.eduplex.eu/impressum/',
                'contact' => [
                    'name' => 'Eduplex development in Gitlab',
                    'url' => 'https://gitlab.com/eduplex-api/cake-api-profile',
                ],
                'license' => [
                    'name' => 'MIT License',
                    'url' => 'https://opensource.org/licenses/MIT',
                ],
            ],
            'servers' => [
                ['url' => $url]
            ],
            'tags' => [],
            'paths' => $paths,
            'components' => [
                'securitySchemes' => [
                    'bearerAuth' => [
                        'type' => 'http',
                        'scheme' => 'bearer',
                    ]
                ],
            ],
        ];
    }
}
