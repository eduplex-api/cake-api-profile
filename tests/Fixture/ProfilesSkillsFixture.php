<?php

declare(strict_types = 1);

namespace Profile\Test\Fixture;

use Profile\Lib\Consts\ProfileSkillStatus;
use RestApi\TestSuite\Fixture\RestApiFixture;

class ProfilesSkillsFixture extends RestApiFixture
{
    const LOAD = 'plugin.Profile.ProfilesSkills';

    public $records = [
        [
            'id' => 1,
            'user_profile_id' => UserProfilesFixture::USER_PROFILE_ID,
            'skill' => 'Spacial vision',
            'esco_uri' => 'http://data.europa.eu/esco/occupation/8b6388a4-4904-471b-9331-d3b1211f5525',
            'status' => ProfileSkillStatus::WANT,
            'created' => '2023-07-12 17:00:00',
            'modified' => '2023-07-12 17:00:00',
            'deleted' => null
        ],
        [
            'id' => 2,
            'user_profile_id' => UserProfilesFixture::USER_PROFILE_ID,
            'skill' => 'Logic',
            'esco_uri' => 'http://data.europa.eu/esco/occupation/8b6388a4-4904-471b-9331-d3b1211f5525',
            'status' => ProfileSkillStatus::HAVE,
            'created' => '2023-07-12 17:00:00',
            'modified' => '2023-07-12 17:02:00',
            'deleted' => null
        ],
    ];

}
