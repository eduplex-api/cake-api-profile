<?php

declare(strict_types = 1);

namespace Profile\Test\Fixture;

use Profile\Lib\Consts\MainGoals;
use RestApi\TestSuite\Fixture\RestApiFixture;

class UserProfilesFixture extends RestApiFixture
{
    const LOAD = 'plugin.Profile.UserProfiles';
    const USER_ID = 2;
    const USER_PROFILE_ID = 1;

    public $records = [
        [
            'id' => self::USER_PROFILE_ID,
            'user_id' => self::USER_ID,
            'current_occupation' => 'Occupation 1',
            'target_occupation' => 'Programmer',
            'main_goal' => MainGoals::GROW_CURRENT_ROLE,
            'goal_description' => 'Occupation goal 1',
            'profile' => 'Profile of the user with the id 2',
            'industry' => 'IT',
            'level' => '25',
            'created' => '2021-01-18 10:39:23',
            'modified' => '2021-01-18 10:41:31'
        ],
        [
            'id' => 2,
            'user_id' => 4,
            'current_occupation' => 'Occupation 2',
            'target_occupation' => 'Architect',
            'main_goal' => MainGoals::GET_FIRST_JOB,
            'goal_description' => 'Occupation goal 2',
            'profile' => 'Profile of the user with the id 4',
            'industry' => 'Construction',
            'created' => '2021-01-18 10:39:23',
            'modified' => '2021-01-18 10:41:31'
        ],
        [
            'id' => 3,
            'user_id' => 5,
            'current_occupation' => 'Occupation 3',
            'target_occupation' => 'Singer',
            'main_goal' => MainGoals::SWITCH_ROLE,
            'goal_description' => 'Occupation goal 3',
            'profile' => 'Profile of the user with the id 2',
            'industry' => 'Music',
            'created' => '2021-01-18 10:39:23',
            'modified' => '2021-01-18 10:41:31'
        ]
    ];
}
