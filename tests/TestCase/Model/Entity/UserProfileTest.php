<?php

declare(strict_types = 1);

namespace Profile\Test\TestCase\Model\Entity;

use Cake\TestSuite\TestCase;
use Profile\Model\Entity\UserProfile;

class UserProfileTest extends TestCase
{
    public function testGetProfilesSkills()
    {
        $profile = new UserProfile();
        $this->assertEquals([], $profile->getProfilesSkills());
        $this->assertEquals(null, $profile->profiles_skills);
        $profile->profiles_skills = ['xx'];
        $this->assertEquals(['xx'], $profile->getProfilesSkills());
        $this->assertEquals(['xx'], $profile->profiles_skills);
    }
}
