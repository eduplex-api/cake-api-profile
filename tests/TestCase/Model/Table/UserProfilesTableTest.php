<?php

declare(strict_types = 1);

namespace Profile\Test\TestCase\Model\Table;

use Cake\TestSuite\TestCase;
use Profile\Model\Table\UserProfilesTable;
use Profile\Test\Fixture\UserProfilesFixture;

class UserProfilesTableTest extends TestCase
{
    public $fixtures = [
        UserProfilesFixture::LOAD,
    ];

    public function testDeleteAllByUser()
    {
        $table = UserProfilesTable::load();
        $this->assertEquals(3, $table->find()->all()->count());
        $this->assertEquals(1, $table->deleteAllByUser(UserProfilesFixture::USER_ID));
        $this->assertEquals(2, $table->find()->all()->count());
    }
}
