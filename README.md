# Cake-API-Profile

Simple API to manage user profiles

## Works with
CakePHP Plugin to run on top of [cake-rest-api](https://packagist.org/packages/freefri/cake-rest-api).

## Openapi documentation

Swagger UI in [/edu/api/v1/profile/openapi/](https://proto.eduplex.eu/edu/api/v1/profile/openapi/)

## License
The source code for the site is licensed under the [**MIT license**](https://gitlab.com/eduplex-api), which you can find in the [LICENSE](../LICENSE/) file.
