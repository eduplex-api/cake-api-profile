<?php

declare(strict_types = 1);

use Migrations\AbstractMigration;

class ChangeUserProfilesCreateProfilesSkills extends AbstractMigration
{
    public function change(): void
    {
        $userProfilesTable = $this->table(\Profile\ProfilePlugin::getTablePrefix() . 'user_profiles',
            ['collation' => 'utf8mb4_unicode_ci']);
        $userProfilesTable->renameColumn('occupation', 'current_occupation');
        $userProfilesTable->renameColumn('goal', 'goal_description');
        $userProfilesTable->addColumn('target_occupation', 'string', [
            'after' => 'current_occupation',
            'default' => null,
            'limit' => \Phinx\Db\Adapter\MysqlAdapter::TEXT_SMALL,
            'null' => true,
        ]);
        $userProfilesTable->addColumn('industry', 'string', [
            'after' => 'profile',
            'default' => null,
            'limit' => \Phinx\Db\Adapter\MysqlAdapter::TEXT_SMALL,
            'null' => true,
        ]);
        $userProfilesTable->addColumn('main_goal', 'string', [
            'after' => 'target_occupation',
            'default' => null,
            'limit' => \Phinx\Db\Adapter\MysqlAdapter::TEXT_SMALL,
            'null' => true,
        ]);
        $userProfilesTable->update();

        $profilesSkills = $this->table(\Profile\ProfilePlugin::getTablePrefix() . 'profiles_skills',
            ['collation' => 'utf8mb4_unicode_ci']);
        $profilesSkills->addColumn('user_profile_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $profilesSkills->addColumn('skill', 'string', [
            'default' => null,
            'limit' => \Phinx\Db\Adapter\MysqlAdapter::TEXT_SMALL,
            'null' => false,
        ]);
        $profilesSkills->addColumn('esco_uri', 'string', [
            'default' => null,
            'limit' => \Phinx\Db\Adapter\MysqlAdapter::TEXT_SMALL,
            'null' => false,
        ]);
        $profilesSkills->addColumn('status', 'string', [
            'default' => null,
            'limit' => \Phinx\Db\Adapter\MysqlAdapter::TEXT_SMALL,
            'null' => false,
        ]);
        $profilesSkills->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $profilesSkills->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $profilesSkills->addColumn('deleted', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $profilesSkills->create();
    }
}
