<?php
declare(strict_types=1);

use App\Model\Table\AppTable;
use Migrations\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CreateUserProfiles extends AbstractMigration
{
    public function change()
    {
        $table = $this->table(\Profile\ProfilePlugin::getTablePrefix() . 'user_profiles',
            ['collation' => 'utf8mb4_unicode_ci']);
        $table->addColumn('user_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('occupation', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('goal', 'text', [
            'default' => null,
            'limit' => MysqlAdapter::TEXT_REGULAR,
            'null' => true,
        ]);
        $table->addColumn('profile', 'text', [
            'default' => null,
            'limit' => MysqlAdapter::TEXT_REGULAR,
            'null' => true,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('deleted', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->create();
    }
}
