<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AddLevelToUserProfiles extends AbstractMigration
{
    public function change(): void
    {
        $userProfilesTable = $this->table(\Profile\ProfilePlugin::getTablePrefix() . 'user_profiles',
            ['collation' => 'utf8mb4_unicode_ci']);
        $userProfilesTable->addColumn('level', 'integer', [
            'after' => 'industry',
            'default' => null,
            'limit' => \Phinx\Db\Adapter\MysqlAdapter::INT_SMALL,
            'null' => true,
        ]);
        $userProfilesTable->update();
    }
}
